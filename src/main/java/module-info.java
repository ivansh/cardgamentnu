module no.ntnu.idatt2001.ivansh.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens no.ntnu.idatt2001.ivansh.cardgame to javafx.fxml;
    exports no.ntnu.idatt2001.ivansh.cardgame;
}