package no.ntnu.idatt2001.ivansh.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


import java.io.IOException;

/**
 * GUI for application
 * @author ivansh
 * @version 2022-03-21
 */
public class CardGameGUI extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CardGameGUI.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 705, 480);
        stage.setResizable(false);
        stage.setTitle("CardGame.Ivansh");
        //solvation how to make a folder with all cards
        stage.getIcons().add(new Image(CardGameGUI.class.getResourceAsStream("/icon.png")));


        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}