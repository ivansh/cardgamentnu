package no.ntnu.idatt2001.ivansh.cardgame;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which administrate our hand with cards
 * this will have collection of certain amount of card
 * which we are holding "in our hand"
 * we need it to check if we have some combinations in our hand
 * @author ivansh
 * @version 2022.03.17
 */
public class HandWithCards {
    private List<PlayingCard> handWithCards = new ArrayList<>(); //collection of cards which we have in our hand

    /**
     * Standard constructor of handWithCardsInstance
     * @param handWithCards list
     */
    public HandWithCards(List<PlayingCard> handWithCards) {
        this.handWithCards = handWithCards;
    }

    /**
     * Method accessor to our hand
     * @return collection with cards which we have in hand
     */
    public List<PlayingCard> getHandWithCards(){
        return handWithCards;
    }

    /**
     * Methode which checks if we have flush in our hand or not
     * @return true or false depends on combination in hand
     */
    public boolean isFlush(){
        if (handWithCards.size()==5) {
            return ((handWithCards.stream().allMatch(playingCard -> playingCard.getSuit() == 'S')) ||
                    (handWithCards.stream().allMatch(playingCard -> playingCard.getSuit() == 'H')) ||
                    (handWithCards.stream().allMatch(playingCard -> playingCard.getSuit() == 'D')) ||
                    (handWithCards.stream().allMatch(playingCard -> playingCard.getSuit() == 'C')));
        }else return false;
    }



}
