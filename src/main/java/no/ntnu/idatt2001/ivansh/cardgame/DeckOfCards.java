package no.ntnu.idatt2001.ivansh.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class which administrate deck of card object
 * There should be 13 cards of every suit
 * That means that there are 13*4= 52 cards in deck
 * @author ivansh
 * @version 2022-03-17
 */
public class DeckOfCards {

    private final List<Character> suitAsList = new ArrayList<>(List.of('S', 'H', 'D', 'C')); //list of 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private List<PlayingCard> deckOfCards= new ArrayList<>(); //collection with all cards

    /**
     * Filling of instance DeckOfCards with 52 cards
     */
    public DeckOfCards() {
        for (int i=1; i<=13;i++){
            int finalI = i;
            suitAsList.forEach((p)->deckOfCards.add(new PlayingCard(p, finalI)));
        }
    }

    /**
     * Method accessor for collection with 52 cards
     * @return collection of 52 cards
     */
    public List<PlayingCard> getDeckOfCards(){return deckOfCards;}

    /**
     * Method which deal hand of user with a certain
     * amount of random playing cards
     * @param i how many random cards do we need
     * @return hand of user with i-amount playing cards
     * @throws IllegalArgumentException if amount of playing cards out of range
     */
    public List<PlayingCard> dealHand(int i) throws IllegalArgumentException{

        if ((i>deckOfCards.size())||(i<=0)) throw new IllegalArgumentException("Illegal number of cards (should be between 1 and remaining deck`s size(firstly 52))");

        else if (i==deckOfCards.size()) {
            List<PlayingCard> handOfUser = new ArrayList<>(deckOfCards);
            deckOfCards.clear();
            return handOfUser;
        }

        else{
            List<PlayingCard> handOfUser = new ArrayList<>();
            Random rand = new Random();
            for (int j=0 ; j<i; j++)   {
                int n = rand.nextInt(52-j);
                handOfUser.add(deckOfCards.get(n));
                deckOfCards.remove(n);
            }return handOfUser;
        }

    }

    /**
     * Method which create refresh deck of cards
     * it make collection with cards empty,
     * and after it fill it up again (the same cycle as in constructor)
     */
    public void refreshDeckOfCards() {
        deckOfCards.clear();
        for (int i=1; i<=13;i++){
            int finalI = i;
            suitAsList.forEach((p)->deckOfCards.add(new PlayingCard(p, finalI)));
        }
    }


}
