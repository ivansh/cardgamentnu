package no.ntnu.idatt2001.ivansh.cardgame;

import java.util.concurrent.atomic.AtomicReference;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;


/**
 * Controller for application
 * @author ivansh
 * @version 2022-03-21
 */
public class CardGameController {

    @FXML
    private ImageView cardFive;

    @FXML
    private ImageView cardFour;

    @FXML
    private ImageView cardOne;

    @FXML
    private ImageView cardThree;

    @FXML
    private ImageView cardTwo;

    @FXML
    private Button checkHandButton;

    @FXML
    private Button dealHandButton;

    @FXML
    private TextField textCards;

    @FXML
    private TextField textFlush;

    @FXML
    private TextField textHearts;

    @FXML
    private TextField textQueen;

    @FXML
    private TextField textSum;

    @FXML
    void initialize() {
        DeckOfCards deckOfCards=new DeckOfCards();

        //AtomicReference helps us to update hand with pushing the button
        AtomicReference<HandWithCards> handWithCards = new AtomicReference<>();

        //Actions that create pressing the "Deal Hand" button
        dealHandButton.setOnAction(actionEvent -> {

            handWithCards.set(new HandWithCards(deckOfCards.dealHand(5)));

            textCards.clear(); // we clean text field with card before we fill it with new string

            handWithCards.get().getHandWithCards().stream()
                    .forEach(playingCard -> textCards.appendText(playingCard.getAsString()+ " "));

            deckOfCards.refreshDeckOfCards(); //refresh Deck Of Card for possibility to use the same deckOfCards again

            //Set Images in centre of application depends on which cards it is
            cardOne.setImage(handWithCards.get().getHandWithCards().get(0).getAsImage());
            cardTwo.setImage(handWithCards.get().getHandWithCards().get(1).getAsImage());
            cardThree.setImage(handWithCards.get().getHandWithCards().get(2).getAsImage());
            cardFour.setImage(handWithCards.get().getHandWithCards().get(3).getAsImage());
            cardFive.setImage(handWithCards.get().getHandWithCards().get(4).getAsImage());

        });

        //Actions after check hand button
        checkHandButton.setOnAction(actionEvent -> {
            //We clear text with all Hearts card, because we use append
            textHearts.clear();

            //Add all Hearts cards in text field
            handWithCards.get().getHandWithCards().stream().forEach(playingCard -> {
                if (playingCard.getSuit()=='H') textHearts.appendText(playingCard.getAsString());
            });

            //Set text field with sum of all cards
            textSum.setText(String.valueOf(handWithCards.get().getHandWithCards().stream()
                    .mapToInt((face)-> face.getFace()).sum()));

            //Only define if we have flush in hand if true - we set text Yes, else - No
            if (handWithCards.get().isFlush()) textFlush.setText("Yes");
            else textFlush.setText("No");

            //If from our hand there is Playing Card face 12 and suit S we write Yes, else - No
            if (handWithCards.get().getHandWithCards().stream()
                    .anyMatch(playingCard -> (playingCard.getFace()==12) && (playingCard.getSuit()=='S')))
                textQueen.setText("Yes");
            else textQueen.setText("No");

        });

    }

}
