package no.ntnu.idatt2001.ivansh.cardgame;

import javafx.scene.image.Image;

/**
 * Represents a playing card. A playing card has a number (face) between
 * 1 and 13, where 1 is called an Ace, 11 = Knight, 12 = Queen and 13 = King.
 * The card can also be one of 4 suits: Spade, Heart, Diamonds and Clubs.
 *
 * @author ntnu
 * @version 2020-01-10
 */
public class PlayingCard{

    private final char suit; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int face; // a number between 1 and 13

    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single character. 'S' for Spades,
     *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
     * @param face The face value of the card, an integer between 1 and 13
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for Clubs
     *
     * @return the suit of the card
     */
    public char getSuit() {
        return suit;
    }

    /**
     * Returns the face of the card (value between 1 and 13).
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }

    /**
     * Method which return every card like image
     * it takes the name of card
     * and depends on it return image of card
     * which stays in resources/cards
     * @return Image with card
     */
    public Image getAsImage(){
        switch (getAsString()){
            case "H1": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/H1.png"));
            case "H2": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/H2.png"));
            case "H3": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h3.png"));
            case "H4": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h4.png"));
            case "H5": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h5.png"));
            case "H6": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h6.png"));
            case "H7": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h7.png"));
            case "H8": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h8.png"));
            case "H9": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h9.png"));
            case "H10": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h10.png"));
            case "H11": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h11.png"));
            case "H12": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h12.png"));
            case "H13": return new Image(PlayingCard.class.getResourceAsStream("/cards/h/h13.png"));

            case "D1": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d1.png"));
            case "D2": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d2.png"));
            case "D3": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d3.png"));
            case "D4": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d4.png"));
            case "D5": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d5.png"));
            case "D6": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d6.png"));
            case "D7": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d7.png"));
            case "D8": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d8.png"));
            case "D9": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d9.png"));
            case "D10": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d10.png"));
            case "D11": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d11.png"));
            case "D12": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d12.png"));
            case "D13": return new Image(PlayingCard.class.getResourceAsStream("/cards/d/d13.png"));

            case "S1": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s1.png"));
            case "S2": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s2.png"));
            case "S3": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s3.png"));
            case "S4": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s4.png"));
            case "S5": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s5.png"));
            case "S6": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s6.png"));
            case "S7": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s7.png"));
            case "S8": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s8.png"));
            case "S9": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s9.png"));
            case "S10": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s10.png"));
            case "S11": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s11.png"));
            case "S12": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s12.png"));
            case "S13": return new Image(PlayingCard.class.getResourceAsStream("/cards/s/s13.png"));

            case "C1": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c1.png"));
            case "C2": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c2.png"));
            case "C3": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c3.png"));
            case "C4": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c4.png"));
            case "C5": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c5.png"));
            case "C6": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c6.png"));
            case "C7": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c7.png"));
            case "C8": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c8.png"));
            case "C9": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c9.png"));
            case "C10": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c10.png"));
            case "C11": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c11.png"));
            case "C12": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c12.png"));
            case "C13": return new Image(PlayingCard.class.getResourceAsStream("/cards/c/c13.png"));


        }return null;



    }
}

