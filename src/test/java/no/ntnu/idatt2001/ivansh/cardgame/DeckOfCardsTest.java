package no.ntnu.idatt2001.ivansh.cardgame;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    DeckOfCards deckOfCards = new DeckOfCards();

    @Test
    public void testOfSize() {
        assertEquals(deckOfCards.getDeckOfCards().size(), 52);
    }

    @Test
    public void testNameOfFirstCard() {
        assertEquals(deckOfCards.getDeckOfCards().get(0).getAsString(), "S1");
    }

    @Test
    public void testNameOfLastCard() {
        assertEquals(deckOfCards.getDeckOfCards().get(51).getAsString(), "C13");
    }
    @Nested
    public class testsOfDealHandMethode{
        @Test
        public void only3CardsIsTakenFromDeckOfCards(){
            DeckOfCards deckOfCards= new DeckOfCards();
            assertEquals(deckOfCards.dealHand(3).size()+deckOfCards.getDeckOfCards().size(),52);
        }
        @Test
        public void allCardsIsTakenFromDeckOfCards(){
            DeckOfCards deckOfCards= new DeckOfCards();
            assertEquals(deckOfCards.dealHand(52).size(),52);
            assertEquals(deckOfCards.getDeckOfCards().size(),0);

        }
        @Test
        public void threeCardsIsTakenFromDeckOfCardsAndAfterAllCards(){
            DeckOfCards deckOfCards = new DeckOfCards();
            deckOfCards.dealHand(3);
            assertEquals(deckOfCards.dealHand(49).size(),49);
            assertEquals(deckOfCards.getDeckOfCards().size(),0);

        }
        @Test
        public void nullIsEnteredAsI(){
            DeckOfCards deckOfCards = new DeckOfCards();
            try {
                deckOfCards.dealHand(0);

            }catch (IllegalArgumentException exception){
                assertEquals(exception.getMessage(),"Illegal number of cards (should be between 1 and remaining deck`s size(firstly 52))");
            }
        }
        @Test
        public void negativeNumberIsEnteredAsI(){
            DeckOfCards deckOfCards = new DeckOfCards();
            try {
                deckOfCards.dealHand(-5);

            }catch (IllegalArgumentException exception){
                assertEquals(exception.getMessage(),"Illegal number of cards (should be between 1 and remaining deck`s size(firstly 52))");
            }
        }
        @Test
        public void numberOutOfBoundsIsEnteredAsI(){
            DeckOfCards deckOfCards = new DeckOfCards();
            try {
                deckOfCards.dealHand(1);// now we have only 51 cards in deck
                deckOfCards.dealHand(52);

            }catch (IllegalArgumentException exception){
                assertEquals(exception.getMessage(),"Illegal number of cards (should be between 1 and remaining deck`s size(firstly 52))");
            }
        }

    }
    @Test
    public void testRefreshDeckOfCards(){
        deckOfCards.dealHand(3);
        assertEquals(deckOfCards.getDeckOfCards().size(),49);
        deckOfCards.refreshDeckOfCards();
        assertEquals(deckOfCards.getDeckOfCards().size(),52);
    }

}
