package no.ntnu.idatt2001.ivansh.cardgame;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;   

public class HandWithCardsTest {

    @Nested
    public class testIsFlush{

        @Test
        public void isNotFlush(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('S',1),
                    new PlayingCard('H',2),
                    new PlayingCard('D', 3),
                    new PlayingCard('C',4),
                    new  PlayingCard('H',5)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertFalse(handWithCards.isFlush());
        }
        @Test
        public void isFlushForS(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('S',1),
                    new PlayingCard('S',2),
                    new PlayingCard('S', 3),
                    new PlayingCard('S',4),
                    new PlayingCard('S',5)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertTrue(handWithCards.isFlush());
        }
        @Test
        public void isFlushForH(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('H',1),
                    new PlayingCard('H',2),
                    new PlayingCard('H', 3),
                    new PlayingCard('H',4),
                    new PlayingCard('H',5)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertTrue(handWithCards.isFlush());
        }
        @Test
        public void isFlushForD(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('D',1),
                    new PlayingCard('D',2),
                    new PlayingCard('D', 3),
                    new PlayingCard('D',4),
                    new PlayingCard('D',5)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertTrue(handWithCards.isFlush());
        }
        @Test
        public void isFlushForC(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('C',1),
                    new PlayingCard('C',2),
                    new PlayingCard('C', 3),
                    new PlayingCard('C',4),
                    new PlayingCard('C',5)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertTrue(handWithCards.isFlush());
        }
        @Test
        public void threeTheSameButOneIsNot(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('C',1),
                    new PlayingCard('C',2),
                    new PlayingCard('C', 3),
                    new PlayingCard('C',4),
                    new PlayingCard('S',4)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertFalse(handWithCards.isFlush());
        }
        @Test
        public void fourSameCards(){
            List<PlayingCard> listOfCardsInHand = new ArrayList<PlayingCard>(List.of(new PlayingCard('C',1),
                    new PlayingCard('C',2),
                    new PlayingCard('C', 3),
                    new PlayingCard('C',4)));
            HandWithCards handWithCards=new HandWithCards(listOfCardsInHand);
            assertFalse(handWithCards.isFlush());

        }
    }
}
